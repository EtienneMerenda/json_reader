import json
from pprint import pprint

class Json_getter():
    """Used for get index of Json or elements in Json"""

    def __init__(self, json_):

        # Json given
        self.json_ = json_

        # stage number
        self._stage_number = 0

        # all keys, str format
        self._keys = ""

        # Keys once time
        self._once_keys = []

        # Booléan define if .txt is generated at end of index or not
        self.txt = True

    def _last_loop(self, dict:dict):
        """Create iterator to check if is last loop"""

        iterator = iter(dict)
        next_ = next(iterator)

        for key in iterator:

            # yield key
            yield next_, False
            # get next key
            next_ = key

        # yield last key
        yield next_, True


    def _stage(self, direction:"up or down"):
        """change & indicate json level"""
        if direction == "down":

            # Change level number
            self._stage_number += 1
            #print(f"\nDown\nLevel: {self._stage_number}")

        elif direction == "up":

            # Change level number
            self._stage_number -= 1
            if self._stage_number > 0:

                #print(f"\nUp\nLevel: {self._stage_number}")
                pass

            # if self.stage == 0. Parsing is ended.
            else:
                print("\n-------------- End parsing --------------", end="\n\n")


    def keys_list(self):
        """Write all keys in .txt file"""

        with open('json_index.txt', 'w') as file:
            file.write(self._keys)


    def get_index(self, json_:json=None):
        """Get all keys in json file"""

        if json_ is None:
            json_ = self.json_

        self._stage('down')

        for key, last in self._last_loop(json_):

            staged_key = f"{'    '*(self._stage_number-1)}{key}\n"
            # Append _keys dictionnary for browse
            self._keys += staged_key

            if staged_key not in self._once_keys:
                self._once_keys.append(staged_key)

            if isinstance(json_[key], dict):
                self.get_index(json_[key])

            if last:
                self._stage('up')

        if self.txt:
            self.keys_list()

    def get_staged_index(self, stage:list, json_:json=None):
        """Get all keys in specific stage of json"""

        if json_ is None:
            json_ = self.json_

        self._stage('down')

        print(self._stage_number)

        for key, last in self._last_loop(json_):

            if self._stage_number in stage:

                staged_key = f"{'    '*(self._stage_number-1)}{key}\n"
                # Append _keys dictionnary for browse
                self._keys += staged_key

                if staged_key not in self._once_keys:
                    self._once_keys.append(staged_key)

            if isinstance(json_[key], dict):
                self.get_staged_index(stage, json_[key])

            if last:
                self._stage('up')


    # Need to rework this side of script:
    # I don't understand what I wrote...
    def get_element(self, target:list):
        "Method used for keep targeted elements in Json"

        try:
            self._keys[(target, self._stage_number)].append(target)

        except KeyError as e:
            print("La clef n'existe pas")
            self._keys[(key, self._stage_number)] = [key]

        if isinstance(json_[key], dict):
            self.get_key(json_[key], target)

with open("fdmprinter.def.json", "r") as json_:
    parser = Json_getter(json.loads(json_.read()))
    parser.get_staged_index([2, 4, 5, 6, 7, 8, 9, 10, 11])

pprint(parser._once_keys)
