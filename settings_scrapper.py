# -*- coding: utf-8 -*-
"""Module used for extract all settings of CURA json and get all settings type to
create models.py of Django project."""

import json
from json_reader import CURAjsonparser
import os
import jinja2
from pprint import pprint
from gettext import gettext



class Json_parser():

    def __init__(self):

        self.level = 0
        self.keys = {}

    def last_loop(self, dict:dict):
        """Create iterator to check if is last loop"""

        iterator = iter(dict)
        next = next(iterator)

        for key in iterator:

            yield next, False
            next = key

        return next, True

    def down(self):

        self.level += 1

    def up(self):
        self.level -= 0


    def get_key(self):

        self.down()

        for key in self.json:
            try:
                self.keys[self.level].append(key)
            except KeyError as e:
                print("La clef n'existe pas")
                self.keys[self.level] = [key]

            if key == "settings":
                self.get_key(json_[key])

        print(self.keys)







# Create folders caontains all models
try:
    os.makedirs("models")

except FileExistsError as e:
    print(e)

parser = CURAjsonparser()

parser.get_key()


#with open("test.py", "w") as file:
#    table(raw_settings['settings'], file)

# def table(json, file):
#
#     for key in json:
#
#         for child_key in json[key]:
#
#             # Get label
#             if key == "label":
#                 table_name = child_key
#                 file.write(f'{child_key}: {json[key][child_key]}\n')
#
#             # Get type
#             if key == "type":
#
#                 file.write(f'{key}: {json[table][key]}\n')
#                 if json[table][key] not in all_type:
#                     all_type.append(json[table][key])
#             # Enter in columns
#             if key == "children":
#                 columns(json[table][key], file)
#             else:
#                 pass
#
# def columns(json, file):
#     """Loop in json, write in file"""
#
#     for table in json:
#         for key in json[table]:
#
#             # Get label
#             if key == "label":
#                 table_name = key
#                 file.write(f'{key}: {json[table][key]}\n')
#
#             # Get type
#             if key == "type":
#
#                 file.write(f'{key}: {json[table][key]}\n')
#                 if json[table][key] not in all_type:
#                     all_type.append(json[table][key])
#             # Enter in columns
#             if key == "children":
#                 columns(json[table][key], file)
#             else:
#                 pass


# # loop in settings key
# for table in raw_settings['settings']:
#
#     # loop in meta
#     for meta in raw_settings['settings'][table]:
#
#         if meta == "label":
#             table = meta
#         # Enter in columns
#         elif meta == "children":
#
#             print(raw_settings['settings'][table][meta])
#         else:
#             print(meta)
#
# # with open("log.txt", "w") as file:
# #     file.write(log)
