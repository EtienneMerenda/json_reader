Project created to parse json.


Objectives are:
* get entire index
* get specified floor index
* get keys once



Methods avalaibles now:


`Json_parser._last_loop(dict:dict)`
=> Create iterator to check if is last loop


`Json_parser._stage(direction:"up or down")`
=> Change & indicate json level


`Json_parser._keys_list()`
=> Write json_index.txt with all keys indented by floor


`Json_parser.get_index()`
=> Get all keys in json file


`Json_parser.get_staged_index(stage:list, json_:json=None)`
=> Get all keys in specific stage of json


`Json_parser.get_element(target:list)`
=> WORK IN PROGRESS